**Compile the software stack**

**Note:**	Guide is: git.axiom-evi/unisi-readme

**Note:**	 when you reboot the host system, recompile the software stack!

Let's start:

```
su - $(whoami)
source ~/axiom-evi/settings.sh
cd axiom-evi/scripts
```

if you need to clean something before the compilation there are multiple type of make clean
```
make help
```
then:
```
make MODE=aarch64 DISABLE_INSTR=0 configure
make mrproper
make MODE=aarch64 DISABLE_INSTR=0 configure
make all
make deb
make deb-tests 
cp axiom-evi/debs/* ~/xgenimage/store/rootfs/dpkgs
```

## Generate the image with xgenimage

**Note:**	Guide at : https://wiki.axiom-project.eu/index.php/WP6_-_ARCHITECTURE_IMPLEMENTATION/BSP/XGenImage

got into the folder xgenimage
```
sudo ./xgenimage.sh --defconfig=axiom_ubuntu
```
( to check the configuration and update the bsp , if necessary)
```
sudo ./xgenimage.sh --config 
```

open ~/xgenimage/.config and set CONFIG_IMAGE_PARTITION_ROOT_SIZE=8192

#### to save the configuration in case of new bsp

```
sudo ./xgenimage.sh --exportconfig=my_config

sudo ./xgenimage.sh --full-image ( it takes long time)
```
**Note:**	 if error happens, check the log. A possible error could be that the repository fails. Check dns and put google dns in resolv.conf to be sure. 

**Note:**	Another error could be the unresolved hostname. Check the file /etc/hosts and copy the configuration of the localhost.localdomain from another machine of the lab. 

**Note:**	Some packages are actually unnecessary and during the installation they faced error, so you can eliminate them in the xgenimage scripts, 
Also, if you encountered a security problem with ubuntu Ver. 16.04.06 thats because of the security package, you can easily avoid any update during the installation again with doing comment some lines in xgenimage scripts, 
PATH: /home/lab223/xgenimage/store/rootfs/patch_pre_install
and you can avoid any POST install action by commenting the related line in xgenimage.sh


Load image into the SD card
```
sudo dd bs=1M if=~/xgenimage/axiom_image.img of=/dev/sdb status=progress
```
NOTE: check if the device is /dev/sdb (SDcard often appear in sdb format in dmesg you can check it)
```
sudo sync
```
mount the device

Load the hardware implementation if it is necessary

```
sudo cp ~/bsp/AXIOM-ZU9EG-2016.3/images/linux/BOOT.BIN /media/partition1
sudo cp ~/bsp/AXIOM-ZU9EG-2016.3/images/linux/image.ub /media/partition1
```
umount the device

If OS version is 16.04.6 fix the ssh bug:
```
sudo vi /etc/rc.local
```
add the following line of code before “exit 0” line: 
```
if [ ! -d /var/run/sshd ]; then
mkdir /var/run/sshd
chmod 0755 /var/run/sshd
fi
```
reboot system


### Modify library and install it


recompile the software stack and generate the deb file ( see cap 1)
```
scp ~/axiom-evi/debs/DEBFILE ubuntu@IPOFBOARD: ( or mount the sd card and copy the deb file in /home/ubuntu)
sudo dpkg -i name.deb
```


## BSP

**Note:**	 If it is not the first compilation you can skip to number 6
**Note:**	 if you need just to update bitstream, compile just the BSP ( image re-generation is not necessary, just move BOOT.bin and image.ub in the partition of the SD CARD named bootfs -> please use terminal, avoid graphical move and first delete and after copy. Sudo sync at the end). The files are located in bsp/AXIOM-ZU9EG-2016.3/images/linux

Install petalinux

```
mkdir ~/petalinux

wget --no-check-certificate --user=YOUR_USERNAME --ask-password https://upload.axiom-project.eu/uploads/WP5.1/tools/petalinux-v2016.3-final-installer.run

chmod +x petalinux-v2016.3-final-installer.run

./petalinux-v2016.3-final-installer.run ~/petalinux
```

download the petalinux project
```
git clone https://git-private.axiom-project.eu/AXIOM-ZU9EG-2016.3/
```

prepare the environment

.move .hdf and .bit files into the

$DESTFOLDER: bsp/AXIOM-ZU9EG-2016.3/hardware/AXIOM_ni/

move into the petalinux project folder

```
cd ~/bsp/AXIOM-ZU9EG-2016.3

source ~/petalinux/settings.sh ~/petalinux

petalinux-config --get-hw-description ./hardware/AXIOM_ni/
```

#### build BSP

**Note:**	if you are not using XSMLL bitstream, you must have to rename nic128 in nic of these elements:
/bsp/.../subsystems/linux/configs/device-tree/system-top.dts

    • registers
    • fifo-raw-tx
    • fifo-raw-rx
    • fifo-rdma-tx
    • fifo-rdma-rx
    • bram-long-buf
    • bram-routing
    • TxRawFifo
    • RxRawFifo
    • TxDMAFifo
    • RxDMAFifo

```
petalinux-build
```
Creating kernel and uboot
```
petalinux-package --image
```
Add bitstream to FSBL
```
petalinux-package --boot --u-boot --fpga ./hardware/AXIOM_ni/main_design_wrapper.bit --force
```
Create final package
```
petalinux-package --prebuilt --force
petalinux-package --bsp -p ./ --output ../name_of_my_new_bsp_package
```
**Tip:**
if you need to run a fresh compilation, first clean with the command: petalinux-build -x distclean
